<?php

/**
 * @file
 * This file includes core functionality.
 */

/**
 * Class DsMetaDatabase.
 */
class DsMetaDatabase {

  /**
   * Associate the view_mode with the entity_type.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $view_mode
   *   The view mode.
   */
  public static function dsMEtaAddEntityMetaViewMode($entity_type, $view_mode) {
    db_insert('ds_meta_entity_meta_view_modes')
      ->fields(array(
        'entity_type' => $entity_type,
        'meta_view_mode' => $view_mode,
      ))
      ->execute();
  }

  /**
   * Removes a meta_view_mode.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $view_mode
   *   The view mode.
   */
  public static function dsMetaRemoveEntityMetaViewMode($entity_type, $view_mode) {
    db_delete('ds_meta_entity_meta_view_modes')
      ->condition('entity_type', $entity_type)
      ->condition('meta_view_mode', $view_mode)
      ->execute();

    db_delete('ds_meta_bundle_view_modes')
      ->condition('entity_type', $entity_type)
      ->condition('meta_view_mode', $view_mode)
      ->execute();

    db_delete('ds_meta_bundle')
      ->condition('entity_type', $entity_type)
      ->condition('meta_view_mode', $view_mode)
      ->execute();
  }

  /**
   * Check if the view_mode is a meta_view_mode.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $view_mode
   *   The view mode.
   *
   * @return bool
   *   TRUE if view mode is selected as a meta vew mode. FALSE if not.
   */
  public static function dsMetaIsMetaViewMode($entity_type, $view_mode) {
    $query = db_select('ds_meta_entity_meta_view_modes', 'dsm_emvm')
      ->fields('dsm_emvm')
      ->condition('entity_type', $entity_type)
      ->condition('meta_view_mode', $view_mode)
      ->execute();
    $counts = $query->rowCount();

    return ($counts > 0);
  }

  /**
   * Returns all meta view modes.
   *
   * @param string $entity_type
   *   Filter meta_view_modes if an entity type is passed by argument.
   *
   * @return array
   *   All meta view modes used.
   */
  public static function dsMetaGetMetaViewModes($entity_type = NULL) {
    $query = db_select('ds_meta_entity_meta_view_modes', 'dsm_emvm');
    $query->fields('dsm_emvm', array('entity_type', 'meta_view_mode'));
    if ($entity_type != NULL) {
      $query->condition('entity_type', $entity_type);
    }
    $results = $query->execute();
    $data = $results->fetchAll();
    $array_data = array();
    foreach ($data as $meta_view_mode) {
      $array_data[$meta_view_mode->entity_type][] = $meta_view_mode->meta_view_mode;
    }

    return $array_data;
  }

  /**
   * Removes the meta view mode for the bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle for the entity.
   * @param string $meta_view_mode
   *   The meta view mode.
   */
  public static function dsMetaPrepareBundles($entity_type, $bundle, $meta_view_mode) {
    db_delete('ds_meta_bundle_view_modes')
      ->condition('entity_type', $entity_type)
      ->condition('bundle', $bundle)
      ->condition('meta_view_mode', $meta_view_mode)
      ->execute();
  }

  /**
   * Adds a new view_mode for an entity_type, bundle and meta_view_mode.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle for the entity.
   * @param string $meta_view_mode
   *   The meta view mode.
   * @param string $view_mode
   *   The view mode.
   */
  public static function dsMetaAddBundleViewMode($entity_type, $bundle, $meta_view_mode, $view_mode) {
    db_insert('ds_meta_bundle_view_modes')
      ->fields(array(
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'meta_view_mode' => $meta_view_mode,
        'view_mode' => $view_mode,
      ))
      ->execute();
  }

  /**
   * Returns all view_modes from a specific bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle for the entity.
   * @param string $meta_view_mode
   *   The meta view mode.
   *
   * @return array
   *   All view modes from the bundle.
   */
  public static function dsMetaGetViewModesFromBundle($entity_type, $bundle, $meta_view_mode = NULL) {
    $query = db_select('ds_meta_bundle_view_modes', 'dsm_bvm');
    $query->fields('dsm_bvm');
    $query->condition('entity_type', $entity_type);
    $query->condition('bundle', $bundle);
    if ($meta_view_mode != NULL) {
      $query->condition('meta_view_mode', $meta_view_mode);
    }

    $results = $query->execute()
                     ->fetchAll();
    return $results;
  }

  /**
   * Adds a view mode for a specific entity_id.
   *
   * @param string $entity_id
   *   Entity id.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle of the entity.
   * @param string $meta_view_mode
   *   Meta view mode associated to this entity id.
   * @param string $view_mode
   *   View mode associated to this entity id.
   */
  public static function dsMetaSaveViewModeForBundle($entity_id, $entity_type, $bundle, $meta_view_mode, $view_mode) {
    db_merge('ds_meta_bundle')
      ->key(array(
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'meta_view_mode' => $meta_view_mode,
      ))
      ->fields(array(
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'bundle' => $bundle,
        'meta_view_mode' => $meta_view_mode,
        'view_mode' => $view_mode,
      ))
      ->execute();
  }

  /**
   * Returns the view mode for this specific node.
   *
   * @param string $entity_id
   *   Entity id.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle of the entity.
   * @param string $meta_view_mode
   *   Meta view mode associated to this entity id.
   *
   * @return string
   *   The view mode
   */
  public static function dsMetaGetViewModeForBundle($entity_id, $entity_type, $bundle, $meta_view_mode) {
    $results = db_select('ds_meta_bundle', 'dsm_b')
      ->fields('dsm_b')
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->condition('bundle', $bundle)
      ->condition('meta_view_mode', $meta_view_mode)
      ->execute()
      ->fetchAssoc();
    return (!empty($results) ? $results['view_mode'] : 'none');
  }

  /**
   * Remove a view mode from an specific entity_id.
   *
   * @param string $entity_id
   *   Entity id.
   * @param string $entity_type
   *   Entity type.
   * @param string $bundle
   *   Bundle of the entity.
   * @param string $meta_view_mode
   *   Meta view mode associated to this entity id.
   */
  public static function dsMetaRemoveViewModeForBundle($entity_id, $entity_type, $bundle, $meta_view_mode) {
    db_delete('ds_meta_bundle')
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->condition('bundle', $bundle)
      ->condition('meta_view_mode', $meta_view_mode)
      ->execute();
  }
}
