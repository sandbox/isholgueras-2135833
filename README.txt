
-- SUMMARY --

Meta Display Suite extends Display Suite.
It allows you to use always view modes (Teaser or Full content) as
"Meta view mode" wherever you want (in views, ...) and overwrite it with other
view modes for each node.

This Meta view mode will be the view mode by default. The view mode which
override.

-- REQUIREMENTS --

Display suite
Display suite UI


-- INSTALLATION --

* Install as usual


-- CONFIGURATION --

* Go to Structure » Display Suite » View modes:

  - Add any view mode as you want

* Go to Structure » Display Suite » Meta view modes:

  - Select which view modes will be setted as "meta view modes". Usually will
    be Teaser and Full content for nodes, but It could be RSS or another one.

* For each content type, go to Structure » Content types » <content_type> »
  Manage Display  »  Custom display settings Tab:

  - Select which view modes will be enabled and Save the form.
  - Click configure in each display selected as meta view mode and select which
    view modes will be selectable for this meta view mode.
  - Configure all view modes.

* Create a node for this content type and change the display mode in Meta
  display for node.


-- CONTACT --

Current maintainers:
* Ignacio Sánchez (Ignacio Sánchez) - https://drupal.org/user/733162

This project has been sponsored by:
* IDEALISTA NEWS
